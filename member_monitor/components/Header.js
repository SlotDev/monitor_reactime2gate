import React, { Component } from 'react';
import Axios from 'axios';
import Link from 'next/link';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { stm1: null, today1: null };
  }

  getDataHeader() {
    Axios.get('http://localhost:8002/react_api/member_monitor/data.php').then(
      res => {
        this.setState({
          stm1: res.data.dataNav[0].stm1,
          today1: res.data.dataNav[0].todayQ
        });
      }
    );
  }

  componentDidMount() {
    this.timeInter = setInterval(() => {
      this.getDataHeader();
    }, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.timeInter);
  }

  render() {
    // console.log(this.state)
    return (
      <div>
        <nav className="navbar is-transparent">
          <div className="navbar-brand">
            <a className="brand-logo">
              <img
                src="/static/images/tu-logo.png"
                alt="Bulma: a modern CSS framework based on Flexbox"
                width="45"
                height="auto"
              />
            </a>
            <ul>
              <li>Thammasat university</li>
              <li>มหาวิทยาลัยธรรมศาสตร์</li>
            </ul>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="field is-grouped">
                <p className="control">
                  <span>
                    <p>
                      จำนวนคนลงทะเบียนวันนี้ {this.state.stm1} ณ วันที่{' '}
                      {this.state.today1}
                    </p>
                  </span>
                </p>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
