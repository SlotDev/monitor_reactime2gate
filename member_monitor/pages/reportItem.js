import React, { Component } from 'react';
import Axios from 'axios';

class ReportItem extends Component {
  constructor(props) {
    super(props);
    this.state = { re_fgs: null };
  }

  getDataContent = () => {
    Axios.get('http://localhost:8002/react_api/member_monitor/data.php').then(
      res => {
        this.setState({ re_fgs: res.data.data });
      }
    );
  };

  componentDidMount() {
    this.timeInter = setInterval(() => {
      this.getDataContent();
    }, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.timeInter);
  }

  render_refgs = re_fgs => {
    // const imageAlert = '/static/images/book-cover-alert.png';
    return (
      this.state.re_fgs &&
      this.state.re_fgs.map(re_fg => {
        return (
          <div key={re_fg.re_sg_id} className="bg-set">
            <section>
              <ul>
                <li className="detail-Gate gate-in">
                  <div className="card">
                    <div className="card-content">
                      <div className="columns is-gapless is-multiline">
                        <div className="column is-one-quarter" align="center">
                          <img
                            src={re_fg.image}
                            className="set-profile"
                            alt="User Avatar"
                          />
                        </div>
                        <div className="column" align="left">
                          <div className="move-text-in">
                            <div>
                              <label>Name (EN)</label>
                              <p>{re_fg.regis_fullnameEn}</p>
                            </div>
                            <div>
                              <label>Name (TH)</label>
                              <p>{re_fg.regis_fullnameTh}</p>
                            </div>
                            <div>
                              <label>Expiry date </label>
                              <p>{re_fg.regis_expire}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </section>
            <style jsx>{`
              .set-div-column {
                background-color: #78ff9c;
              }
              .set-div-column2 {
                background-color: red;
              }
              .set-p {
                margin-top: 25px;
              }
              .move-text-in p {
                margin-top: -8px;
                margin-bottom: -7px;
                color: #737272;
                font-size: 22px;
              }
              .setBorrow {
                font-size: 50px;
                margin: 78px 0px auto 0px;
              }
              .setNot_Borrow {
                font-size: 50px;
                margin: 35px 0px auto 0px;
              }
            `}</style>
          </div>
        );
      })
    );
  };

  render() {
    // console.log(this.state.re_fgs);
    return <div className="row">{this.render_refgs(this.state.re_fgs)}</div>;
  }
}

export default ReportItem;
