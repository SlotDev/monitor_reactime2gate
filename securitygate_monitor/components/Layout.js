import React, { Component } from "react";
import Head from "next/head";
import Header from "./Header";
// import Footer from "./Footer";

class Layout extends Component {
  render() {
    // *children is content of <Layout></Layout> in about.js 
    const { children, title = "SlotDev Bloger" } = this.props
    return (
      <div>
        <Head>
          <title>{ title }</title>
          <meta httpEquiv="Content-Type" content="text/html; charSet=utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="stylesheet" href="/static/vendor/bulma-0.7.1/css/bulma.min.css"></link>
          <link rel="stylesheet" href="/static/css/main.css" />
        </Head>
        <div className="container">
        <Header />
        { children }
        {/* <Footer /> */}
        </div>
      </div>
    )
  }
}

export default Layout;