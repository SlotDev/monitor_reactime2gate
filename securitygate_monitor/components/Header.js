import React, { Component } from 'react';
import Axios from "axios";
import Link from "next/link";

class Header extends Component {

  constructor(props) {
    super(props)
    this.state = { stm1: null, today1: null }
  }

  getDataHeader() {
    Axios.get("http://localhost:8001/react_api/securitygate_monitor/data.php").then( res => {
      this.setState({ stm1: res.data.dataNav[0].stm1, today1: res.data.dataNav[0].todayQ })
    })
  }

  componentDidMount() {
    this.timeInter = setInterval(() => {
      this.getDataHeader()
    }, 2000)
  }

  componentWillUnmount() {
    clearInterval(this.timeInter)
  }

  render() {
    // console.log(this.state)
    return (
      <div>
        <nav className="navbar is-transparent">
          <div className="navbar-brand">
            <a className="brand-logo">
              <img src="/static/images/nstru-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="30" height="auto" />
            </a>
            <ul>
              <li>Rajamangala University of technology Srivijaya</li>
              <li>มหาวิทยาลัยเทคโนโลยีราชมงคลศรีวิชัย</li>
            </ul>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="field is-grouped">
                <p className="control">
                  <span>
                    <p>จำนวนหนังสือที่ผ่านประตูวันนี้ {this.state.stm1} เล่ม ณ วันที่ {this.state.today1}</p>
                  </span>
                </p> 
              </div>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

export default Header;
