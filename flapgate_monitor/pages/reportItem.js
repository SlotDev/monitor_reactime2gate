import React, { Component } from "react";
import Axios from "axios";

class ReportItem extends Component {

  constructor(props) {
    super(props)
    this.state = { re_fgs : null }
  }

  getDataContent = () => {
    Axios.get('http://localhost:8001/react_api/flapgate_monitor/data.php').then( res => {
      this.setState({ re_fgs : res.data.data })
    })
  }

  componentDidMount() {
    this.timeInter = setInterval(() => {
      this.getDataContent()
    }, 2000)
  }

  componentWillUnmount() {
    clearInterval(this.timeInter)
  }

  render_refgs = (re_fgs) => {
    return this.state.re_fgs && this.state.re_fgs.map(re_fg => {
      return (
        <div key={re_fg.re_fg_id} className="bg-set">
          <section>
            <ul>
              <li className="detail-Gate gate-in">
                <div className="card">
                  <div className="card-content">
                    <div className="columns is-gapless is-multiline">
                      {re_fg.re_fg_in_out == "IN" &&
                        <div className="column is-one-quarter" align="center">
                          <img src={re_fg.image} className="set-profile" alt="User Avatar" />
                        </div>
                      }
                      {re_fg.re_fg_in_out == "OUT" &&
                        <div className="column is-one-quarter set-font-out set-div-column" align="center">
                          <p className="set-p">OUT</p>
                        </div>
                      }
                      {re_fg.re_fg_in_out == "IN" &&
                        <div className="column is-half" align="left">
                          <div className="lead-img-in">
                            <ul>
                              <li><img className="lead-text" src="/static/images/id.gif" /></li>
                              <li><img className="lead-text" src="/static/images/person.gif" /></li>
                              <li><img className="lead-text" src="/static/images/time.gif" /></li>
                            </ul>
                          </div>
                          <div className="move-text-in">
                            <div>
                              <label>รหัส</label>
                              <p>{re_fg.re_fg_mem_id}</p>
                            </div>
                            <div>
                              <label>ชื่อ-สกุล</label>
                              <p>{re_fg.re_fg_mem_name}</p>
                            </div>
                            <div>
                              <label>เวลา</label>
                              <p>{re_fg.timedff} mins ago</p>
                            </div>
                          </div>
                        </div>
                      }
                      {re_fg.re_fg_in_out == "OUT" &&
                        <div className="column is-half" align="right">
                          <div className="lead-img-out" align="right">
                            <ul>
                              <li><img className="lead-text" src="/static/images/id.gif" /></li>
                              <li><img className="lead-text" src="/static/images/person.gif" /></li>
                              <li><img className="lead-text" src="/static/images/time.gif" /></li>
                            </ul>
                          </div>
                          <div className="move-text-out">
                            <div>
                              <label>รหัส</label>
                              <p>{re_fg.re_fg_mem_id}</p>
                            </div>
                            <div>
                              <label>ชื่อ-สกุล</label>
                              <p>{re_fg.re_fg_mem_name}</p>
                            </div>
                            <div>
                              <label>เวลา</label>
                              <p>{re_fg.timedff} mins ago</p>
                            </div>   
                          </div>
                        </div>
                      }
                      {re_fg.re_fg_in_out == "IN" &&
                        <div className="column is-one-quarter set-font-in set-div-column" align="center">
                          <p>IN</p>
                        </div>
                      }
                      {re_fg.re_fg_in_out == "OUT" &&
                        <div className="column is-one-quarter" align="center">
                          <img src={re_fg.image} className="set-profile" alt="User Avatar" />
                        </div>
                      }
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </section>
          <style jsx>{`
            .set-div-column {
              background-color: #78ff9c;
            }
            .set-p {
              margin-top: 25px; 
            }              
            .move-text-in p { margin-top: -13px; margin-bottom: -7px; color:#737272; font-size:30px; }
          `}</style>
        </div>
      )
    })
  }

  render() {
    // console.log(this.state.re_fgs)
    return (
        <div className="row">
          {this.render_refgs(this.state.re_fgs)}
        </div>
    )
  }
}

export default ReportItem;